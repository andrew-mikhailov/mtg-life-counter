import React, { Component } from 'react';
import CounterContainer from '../CounterContainer/CounterContainer';
import './App.css';

class App extends Component {
  render() {
    return <div>
      <CounterContainer/>
    </div>
  }
}

export default App;
