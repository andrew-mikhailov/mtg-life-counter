import { List} from 'immutable';

export const HEROLIST = List([
  {
    name: 'Garruk',
    color: 'green',
    class: "garruk"
  },
  {
    name: 'Chandra',
    color: 'red',
    class: "chandra"
  },
  {
    name: 'Liliana',
    color: 'purple',
    class: "liliana"
  },
  {
    name: 'Jace',
    color: 'blue',
    class: "jace"
  },
  {
    name: 'Ajani',
    color: 'white',
    class: "ajani"
  },
  {
    name: 'Gisa',
    color: 'black',
    class: "gisa"
  },
  {
    name: 'Nahiru',
    color: 'white',
    class: "nahiri"
  },
  {
    name: 'Nissa',
    color: 'green',
    class: "nissa"
  }
]);
